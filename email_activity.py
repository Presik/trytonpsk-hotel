# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class EmailActivity(metaclass=PoolMeta):
    __name__ = 'email.activity'

    @classmethod
    def _get_origin(cls):
        origins = super(EmailActivity, cls)._get_origin()
        origins.extend([
            'hotel.booking',
        ])
        return origins

    # @classmethod
    # def get_selection(cls):
    #     options = super(DashApp, cls).get_selection()
    #     options.extend([
    #         ('hotel_planner', 'Planner'),
    #         ('housekeeping', 'Housekeeping'),
    #         ('web_checkin', 'Housekeeping'),
    #     ])
    #     return options
