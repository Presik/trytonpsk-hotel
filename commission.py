# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Commission(metaclass=PoolMeta):
    __name__ = 'commission'
    offset_move_line = fields.Many2One('account.move.line', 'Offset Move Line',
        states={'readonly': True})
