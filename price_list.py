# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval, If
# from trytond.pool import Pool


class HotelPriceList(ModelSQL, ModelView):
    'Hotel Price List'
    __name__ = 'hotel.price_list'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=False)
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
        ])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    # @staticmethod
    # def default_currency():
    #     Company = Pool().get('company.company')
    #     company = Transaction().context.get('company')
    #     if company:
    #         return Company(company).currency.id
