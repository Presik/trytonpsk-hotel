
STATE_BOOKING = [
    ('offer', 'Offer'),
    ('confirmed', 'Confirmed'),
    ('cancelled', 'Cancelled'),
    ('not_show', 'No Show'),
    ('finished', 'Finished'),
]

REGISTRATION_STATE = [
    (None, ''),
    ('pending', 'Pending'),
    ('check_in', 'Check In'),
    ('check_out', 'Check Out'),
    ('no_show', 'No Show'),
]

REASON = [
    ('', ''),
    ('sun_beach', 'Sun & Beach'),
    ('sport', 'Sport'),
    ('adventure', 'Adventure'),
    ('nature', 'Nature'),
    ('cultural', 'Cultural'),
    ('urban', 'Urban'),
    ('rural', 'Rural'),
    ('cruises', 'Cruises'),
    ('health', 'Health'),
    ('thematic_parks', 'Thematic Parks'),
    ('convention', 'Conventions & Meetings'),
    ('wedding', 'Weddings'),
    ('bussiness', 'Bussiness'),
]

GUARANTEE = [
    ('', ''),
    ('payment', 'Payment'),
    ('voucher', 'Voucher'),
    ('credit_card', 'Credit Card'),
    ('letter', 'Letter'),
]

SATISFACTION = [
    ('', ''),
    ('5', 'Excellent'),
    ('4', 'Good'),
    ('3', 'Regular'),
    ('2', 'Bad'),
    ('1', 'Worse'),
]

MEDIA = [
    ('', ''),
    ('phone', 'Phone'),
    ('fax', 'Fax'),
    ('email', 'Email'),
    ('chat', 'Chat'),
    ('walking', 'Walking'),
    ('web', 'Web'),
    ('channel_manager', 'Channel Manager'),
    ('ota', 'OTA'),
    ('other', 'Other'),
]

PLAN = [
    ('no_breakfast', 'No Breakfast'),
    ('all_inclusive', 'All Inclusive'),
    ('bed_breakfast', 'Bed & Breakfast'),
    ('full_american', 'Full American'),
    ('half_american', 'Half American'),
]

COMPLEMENTARY = [
    ('', ''),
    ('in_house', 'In House'),
    ('courtesy', 'Courtesy')
]

INVOICE_STATES = [
    ('', 'None'),
    ('draft', 'Draft'),
    ('validated', 'Validated'),
    ('posted', 'Posted'),
    ('paid', 'Paid'),
    ('cancelled', 'Cancelled'),
]

PAYMENT_METHOD_CHANNEL = [
    ('', ''),
    ('at_destination', 'At Destination'),
    ('ota_collect', 'OTA Collect'),
]

TYPE_DOCUMENT = [
    ('11', 'Registro Civil de Nacimiento'),
    ('12', 'Tarjeta de Identidad'),
    ('13', 'Cedula de Ciudadania'),
    ('21', 'Tarjeta de Extranjeria'),
    ('22', 'Cedula de Extranjeria'),
    ('31', 'NIT'),
    ('41', 'Pasaporte'),
    ('42', 'Tipo de Documento Extranjero'),
    ('47', 'PEP'),
    ('50', 'NIT de otro pais'),
    ('91', 'NUIP'),
    ('', ''),
]

COLOR_BOOKING = {
    'draft': '#9b9f9f',
    'check_in': '#e6bd0f',
    'check_out': '#09a4cd',
    'done': '#315274',
}

COLOR_MNT = {
    'draft': '#e87a7a',
    'confirmed': '#d45757',
    'done': '#d45757',
}
