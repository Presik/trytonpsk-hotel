# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields


class Tag(ModelSQL, ModelView):
    'Hotel Tag'
    __name__ = 'hotel.tag'
    name = fields.Char('Name')


class TagGuest(ModelSQL, ModelView):
    'Tag Guest'
    __name__ = 'hotel.tag.guest'
    tag = fields.Many2One('hotel.tag', 'Tag')
    guest = fields.Many2One('hotel.folio.guest', 'Guest')
