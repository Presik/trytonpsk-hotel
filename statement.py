# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'

    @classmethod
    def _get_source(cls):
        return super(StatementLine, cls)._get_source() + [
            'hotel.booking', 'hotel.folio'
        ]

    @classmethod
    def post_move(cls, lines):
        super(StatementLine, cls).post_move(lines)
        for s in lines:
            if s.invoice and s.move and s.invoice.state != 'paid':
                s.invoice.auto_reconcile()
