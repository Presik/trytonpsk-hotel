# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, Id
from trytond.transaction import Transaction
from trytond.model.exceptions import AccessError
from trytond.i18n import gettext


class Configuration(ModelSQL, ModelView):
    'Hotel Configuration'
    __name__ = 'hotel.configuration'
    booking_sequence = fields.Many2One('ir.sequence', 'Hotel Booking Sequence',
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=',
                Id('hotel',
                    'sequence_type_hotel')),
        ], required=True)
    charge_sequence = fields.Many2One('ir.sequence', 'Charges Sequence',
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=',
                Id('hotel',
                    'sequence_type_hotel')),
        ])
    hotel_service_sequence = fields.Many2One('ir.sequence', 'Service Sequence',
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=',
                Id('hotel',
                    'sequence_type_hotel')),
        ], required=True)
    registration_card_sequence = fields.Many2One('ir.sequence',
        'Registration Card Sequence', domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=',
                Id('hotel',
                    'sequence_type_hotel')),
        ], required=True)
    default_charges = fields.Many2Many('hotel.configuration-product.product',
        'configuration', 'product', 'Default Charges')
    check_in_time = fields.Time('Check In Time', required=True)
    check_out_time = fields.Time('Check Out Time', required=True)
    taxes_exception_rule = fields.Many2One('account.tax.rule',
        'Taxes Exception Rule')
    default_channel_seller = fields.Many2One('hotel.channel', 'Default Channel')
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
        ])
    full_clean_time = fields.Selection([
        ('', ''),
        ('by_time', 'By Time'),
        ('on_check_out', 'On Check Out'),
        ], 'Full Clean Time')
    full_clean_lapse = fields.Integer('Full Clean Lapse', help='In Days')
    age_children_policy = fields.Integer('Age Children Policy', help='In Days')
    cleaning_check_out = fields.Many2One('hotel.room.cleaning_type',
        'Cleaning Check Out')
    cleaning_check_in = fields.Many2One('hotel.room.cleaning_type',
        'Cleaning Check In')
    cleaning_occupied = fields.Many2One('hotel.room.cleaning_type',
        'Cleaning Occupied')
    quarantine_rooms = fields.Numeric('Quarantine Rooms', digits=(2, 0),
        help='In days')
    booking_email = fields.Many2One('email.template', 'Booking Email')
    check_in_email = fields.Many2One('email.template', 'Check-In Email')
    payments_email = fields.Many2One('email.template', 'Payments Email')
    customer_experience_email = fields.Many2One('email.template',
        'Customer Experience Email')
    storage_by_default = fields.Many2One('stock.location', 'Storage By Default',
        domain=[('type', '=', 'storage')], required=False)
    payment_term = fields.Many2One('account.invoice.payment_term',
        'Default Payment Term')
    price_list = fields.Many2One('product.price_list', 'Default Price List')
    children_policies = fields.One2Many('hotel.children_policy',
        'configuration', 'Children Policies')
    nationality = fields.Many2One('country.country', 'Default Nationality')
    country = fields.Many2One('party.country_code', 'Default Country')
    offset_journal = fields.Many2One('account.journal', 'Offset Journal')
    accounting_revenue = fields.Selection([
        ('recognise_revenue', 'Recognise Revenue'),
        ('classic', 'Classic'),
        ], 'Accounting Revenue')
    recognise_account = fields.Many2One('account.account', 'Recognise Account',
        domain=[
            ('company', '=', Eval('company')),
            ('type', '!=', None),
        ], depends=['company'])
    space_booking = fields.Many2One('analytic_account.space', 'Default Space')
    advance_account = fields.Many2One('account.account', 'Advance Account',
        domain=[
            ('company', '=', Eval('company')),
            ('type', '!=', None),
        ], depends=['company'])
    token_siat = fields.Char('Token Siat')
    auto_invoice = fields.Selection([
        ('', ''),
        ('validate_send', 'Validate and Send'),
        ], 'Auto Invoice')

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_accounting_revenue():
        return 'classic'

    @classmethod
    def get_configuration(cls):
        company_id = Transaction().context.get('company')
        if company_id:
            config, = cls.search([
                ('company', '=', company_id)
            ])
            if not config or not config.booking_sequence:
                raise AccessError(
                    gettext('hotel.missing_default_configuration'))
            return config


class ConfigurationProduct(ModelSQL):
    'Configuration - Product'
    __name__ = 'hotel.configuration-product.product'
    _table = 'hotel_configuration_product_rel'
    configuration = fields.Many2One('hotel.configuration', 'Configuration',
        ondelete='CASCADE', select=True, required=True)
    product = fields.Many2One('product.product', 'Product',
        ondelete='RESTRICT', select=True, required=True)


class ChildrenPolicy(ModelSQL, ModelView):
    'Children Policy'
    __name__ = 'hotel.children_policy'
    configuration = fields.Many2One('hotel.configuration', 'Configuration',
        ondelete='CASCADE', required=True)
    limit_age = fields.Integer('Limit Age', help='In years')
    amount = fields.Numeric('Amount')
