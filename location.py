# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class HotelLocation(ModelSQL, ModelView):
    "Hotel Location"
    __name__ = "hotel.location"
    name = fields.Char('Name', required=True, translate=True)
    parent = fields.Many2One('hotel.location', 'Parent')
    childs = fields.One2Many('hotel.location', 'parent', string='Children')

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + self.name
        else:
            return self.name
