# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class MigrationCity(ModelSQL, ModelView):
    "Migration City"
    __name__ = "hotel.migration_city"
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')

    @classmethod
    def __setup__(cls):
        super(MigrationCity, cls).__setup__()
