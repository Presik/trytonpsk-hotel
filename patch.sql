
ALTER TABLE hotel_channel DROP COLUMN commission;
ALTER TABLE hotel_channel DROP COLUMN payment_method;
ALTER TABLE hotel_booking RENAME COLUMN party_seller TO channel;
UPDATE hotel_folio SET nights_quantity=(departure_date-arrival_date)


UPDATE account_statement_line SET source=t.booking FROM (
  SELECT account_statement_line.id AS lid, 'hotel.booking,' || hfst.booking AS booking
    FROM account_statement_line
    INNER JOIN hotel_booking_statement_line_rel AS hfst
      ON hfst.statement_line=account_statement_line.id) AS t
    WHERE t.lid=account_statement_line.id;


DROP TABLE hotel_booking_statement_line_rel;
DROP TABLE hotel_folio_statement_line_rel;



|| ta.id FROM hotel_folio
 WHERE account_invoice_line.origin

 SELECT id, booking FROM hotel_folio

------------------------------------------------------------------

ALTER TABLE account_invoice_line ADD COLUMN origin2 VARCHAR;
UPDATE account_invoice_line SET origin2=origin;

UPDATE account_invoice_line AS ail
  SET origin='hotel.folio,' || st.folio_id FROM (
    SELECT fo.id AS folio_id, fo.booking, ta.line_id AS line_id
    FROM hotel_folio AS fo INNER JOIN (
      SELECT ail.id AS line_id, SPLIT_PART(ail.origin, ',', 2)::INTEGER AS booking_id, ail.origin
      FROM account_invoice_line AS ail
      WHERE origin ILIKE 'hotel.booking%') AS ta
      ON ta.booking_id=fo.booking
  ) AS st
  WHERE ail.id=st.line_id AND ail.origin ILIKE 'hotel.booking%';

ALTER TABLE account_invoice_line DROP COLUMN origin2;
