# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


KIND = [
    ('', ''),
    ('accommodation', 'Accommodation'),
    ('bar_restaurant', 'Bar and Restaurant')
]


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    kind = fields.Selection(KIND, 'Kind')
    channel_code = fields.Char('Channel Code')
    accommodation_capacity = fields.Integer('Accommodation Capacity',
        states={'invisible': Eval('kind') != 'accommodation'}
    )


class PriceList(metaclass=PoolMeta):
    __name__ = 'product.price_list'
    breakfast_included = fields.Boolean('Breakfast Included')
    season = fields.Selection([
            ('high', 'High'),
            ('low', 'Low'),
            ('middle', 'Middle'),
        ], 'Season')
