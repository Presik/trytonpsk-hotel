# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class HotelPolicyCancellation(ModelSQL, ModelView):
    'Hotel Policy Cancellation'
    __name__ = 'hotel.policy.cancellation'
    name = fields.Char('Name', required=True)
