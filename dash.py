# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.dash.dash import DashAppBase


class DashApp(metaclass=PoolMeta):
    __name__ = "dash.app"

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend(
            [
                "dash.app.hotel_planner",
                "dash.app.housekeeping",
                "dash.app.web_checkin",
                "dash.app.booking",
                "dash.app.web_booking",
                "dash.app.web_melhous",
            ]
        )
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend(
            [
                ("hotel_planner", "Planner"),
                ("housekeeping", "Housekeeping"),
                ("web_checkin", "Web Checkin"),
                ("booking", "Booking"),
                ("web_booking", "Web Booking"),
                ("web_melhous", "Web Melhous"),
            ]
        )
        return options


class AppHotelPlanner(DashAppBase):
    "App Hotel Planner"
    __name__ = "dash.app.hotel_planner"


class AppHousekeeping(DashAppBase):
    "App Housekeeping"
    __name__ = "dash.app.housekeeping"


class AppWebCheckIn(DashAppBase):
    "App Web Check-In"
    __name__ = "dash.app.web_checkin"


class AppBooking(DashAppBase):
    "App Booking"
    __name__ = "dash.app.booking"

class AppWebBooking(DashAppBase):
    "App Web Booking"
    __name__ = "dash.app.web_booking"
    
class AppWebMelHous(DashAppBase):
    "App Web Melhous"
    __name__ = "dash.app.web_melhous"
    