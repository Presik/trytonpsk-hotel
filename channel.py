# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from trytond.pool import Pool
from .constants import PAYMENT_METHOD_CHANNEL


class ChannelCommission(ModelSQL, ModelView):
    'Channel Commission Extra'
    __name__ = 'hotel.channel.commission'
    channel = fields.Many2One('hotel.channel', 'Sale Channel',
        ondelete='CASCADE', required=True)
    name = fields.Char('Name', required=True)
    commission = fields.Float('Commission', required=True, digits=(4, 2))


class SaleChannel(ModelSQL, ModelView):
    'Sale Channel'
    __name__ = 'hotel.channel'
    name = fields.Char('Name', required=True)
    code = fields.Selection([
        (None, ''),
        ('booking', 'Booking'),
        ('despegar', 'Despegar'),
        ('expedia', 'Expedia'),
        ('airbnb', 'Airbnb'),
        ('other', 'Other'),
        ], 'Code', required=False)
    agent = fields.Many2One('commission.agent', 'Agent', required=True)
    type_commission = fields.Selection([
            ('percentage', 'Percentage'),
            ('fixed', 'Fixed'),
        ], 'Type Commission', required=True)
    invoice_to = fields.Selection([
            ('channel', 'Channel'),
            ('customer', 'Customer'),
        ], 'Invoice To', required=True)
    # Remove this field
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
        ], select=True)
    commission = fields.Float('Commission', required=True, digits=(4, 2))
    debit_account = fields.Many2One('account.account', 'Debit Account',
        domain=[
            ('company', '=', Eval('company')),
            ('type', '!=', None),
        ],
        depends=['company', 'type_commission'])
    credit_account = fields.Many2One('account.account', 'Credit Account',
        domain=[
            ('company', '=', Eval('company')),
            ('type', '!=', None),
        ],
        depends=['company', 'type_commission'])
    taxes = fields.Many2Many('hotel.channel-account.tax',
        'channel', 'tax', 'Channel Taxes',
        domain=[
            ('parent', '=', None), ['OR',
                ('group', '=', None),
                ('group.kind', 'in', ['purchase', 'both'])],
            ]
        )
    currency = fields.Many2One('currency.currency', 'Currency',
        required=True)
    price_list = fields.Many2One('product.price_list', 'Price List')
    payment_mode = fields.Many2One('account.voucher.paymode',
        'Receipt - Payment Mode', domain=[
            ('company', 'in', [Eval('company', -1), None]),
        ])
    payment_method = fields.Selection(PAYMENT_METHOD_CHANNEL, 'Payment Method')
    collection_mode = fields.Selection([
        ('', ''),
        ('anticipated', 'Anticipated'),
        ('post_checkin', 'Post Checkin'),
        ], 'Collection Mode', required=False,
        help="Commission collection mode")
    extra_commissions = fields.One2Many('hotel.channel.commission',
        'channel', 'Extra Commissions')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.id

    def compute(self, amount):
        res = 0
        if self.currency and self.commission:
            res = self.currency.round(amount * Decimal(self.commission) / 100)
        return res


class ChannelTax(ModelSQL):
    'Channel - Tax'
    __name__ = 'hotel.channel-account.tax'
    _table = 'hotel_channel_taxes_rel'
    channel = fields.Many2One('hotel.channel', 'Sale Channel',
        ondelete='CASCADE', required=True)
    tax = fields.Many2One('account.tax', 'Tax', ondelete='RESTRICT',
        required=True)
