# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import location
from . import configuration
from . import room
from . import booking
from . import folio
from . import company
from . import city
from . import party
from . import channel
from . import product
from . import sale
from . import service
from . import policy
from . import dash
from . import invoice
from . import statement
from . import stock
from . import operation
from . import ir
from . import email_activity
from . import guest
from . import commission
from . import account
from . import analytic
from . import price_list
from . import rate_plan
from . import siat
from . import housekeeping


def register():
    Pool.register(
        ir.Cron,
        room.RoomClassification,
        room.Room,
        room.Amenities,
        room.RoomAmenities,
        room.RoomTemplate,
        city.MigrationCity,
        location.HotelLocation,
        policy.HotelPolicyCancellation,
        company.Company,
        configuration.ConfigurationProduct,
        configuration.Configuration,
        configuration.ChildrenPolicy,
        product.Template,
        product.PriceList,
        operation.Maintenance,
        booking.Booking,
        booking.BookingStatusStart,
        booking.ManagerStart,
        booking.BookingStatementLine,
        booking.StatementPaymentForm,
        statement.StatementLine,
        email_activity.EmailActivity,
        party.Party,
        channel.SaleChannel,
        channel.ChannelTax,
        channel.ChannelCommission,
        booking.SelectRoomsAsk,
        booking.BookingVoucher,
        booking.RoomsOccupancyStart,
        booking.RevenueForecastStart,
        booking.OperationForecastStart,
        booking.RevenueSegmentationStart,
        booking.UpdateHolderStart,
        booking.BookingChannelCommision,
        booking.BillBookingStart,
        booking.UpdateTaxesStart,
        folio.FolioGuest,
        folio.GuestsListStart,
        folio.StatisticsByMonthStart,
        folio.Folio,
        folio.FolioCharge,
        folio.FolioOccupancy,
        folio.HotelFolioTax,
        folio.FolioStockMove,
        folio.OpenMigrationStart,
        folio.UpdateOccupancyStart,
        folio.HotelFolioChargeTax,
        folio.FolioAuditStart,
        guest.Tag,
        guest.TagGuest,
        stock.Move,
        sale.InvoiceIncomeDailyStart,
        sale.Sale,
        sale.SaleLine,
        sale.SaleTransferStart,
        service.Service,
        service.ServiceLine,
        service.ServiceKind,
        service.CreateDailyServicesStart,
        room.CleanningDays,
        room.CleaningType,
        dash.DashApp,
        dash.AppHotelPlanner,
        dash.AppHousekeeping,
        dash.AppWebCheckIn,
        dash.AppBooking,
        dash.AppWebBooking,
        invoice.InvoiceLine,
        invoice.Invoice,
        room.HotelTask,
        room.HousekeepingStart,
        housekeeping.HousekeepingScheduleStart,
        operation.NightAudit,
        operation.NightAuditStart,
        commission.Commission,
        account.Move,
        analytic.AnalyticSpace,
        price_list.HotelPriceList,
        rate_plan.RatePlan,
        rate_plan.RatePlanPriceList,
        rate_plan.RatePlanRule,
        rate_plan.RatePlanCalendar,
        siat.SiatSyncStart,
        dash.AppWebMelHous,
        module='hotel', type_='model')
    Pool.register(
        booking.BookingReport,
        booking.RevenueForecastReport,
        booking.OperationForecastReport,
        booking.RevenueSegmentationReport,
        booking.RoomsOccupancyReport,
        booking.BookingStatusReport,
        booking.ManagerReport,
        booking.BookingStatementReport,
        folio.Migration,
        folio.GuestsListReport,
        folio.RegistrationCardReport,
        folio.StatisticsByMonthReport,
        folio.FolioAuditReport,
        sale.InvoiceIncomeDailyReport,
        sale.InvoiceSimplifiedReport,
        service.ServiceReport,
        room.HousekeepingReport,
        module='hotel', type_='report')
    Pool.register(
        booking.SelectRooms,
        booking.RevenueForecast,
        booking.OperationForecast,
        booking.RevenueSegmentation,
        booking.RoomsOccupancy,
        booking.BookingStatus,
        booking.UpdateHolder,
        booking.Manager,
        booking.WizardStatementPayment,
        booking.BillBooking,
        booking.UpdateTaxes,
        folio.OpenMigration,
        folio.GuestsList,
        folio.StatisticsByMonth,
        folio.ReverseCheckout,
        folio.UpdateOccupancy,
        folio.FolioAudit,
        service.CreateDailyServices,
        room.Housekeeping,
        housekeeping.HousekeepingSchedule,
        sale.InvoiceIncomeDaily,
        sale.SaleTransfer,
        party.CreateGuest,
        operation.NightAuditWizard,
        invoice.InvoiceReconcileBooking,
        siat.SiatSyncWizard,
        module='hotel', type_='wizard')
