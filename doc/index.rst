Hotel Module
###############

The Hotel Management Module.

----- Tryton Solución Hotel -----

Más allá de ser un software hotel potente, abierto, flexible y modular, está diseñado 
para una fácil y completa integración de todas las áreas de la gestion hotelera.

Aplicación para control de las actividades de un hotel: 
- Configuración de habitaciones, salones.
- Tarificación según calendario, Listas de Precios por Clientes corporativos ó Agencias. 
- Gestión de reservas y clientes
- Registro de gastos y facturación.
- Historial de Visitas
- Comunicación con central, informes y facturación.
- Módulo Ama de Llaves, con gestión de estado de limpieza y mantenimiento de habitaciones.
- Gestión de SPA y servicios, con manejo de turnos, control de acceso de personal.
- Módulo Restaurante (costos de produción, recetas, control de stock) e informes de rentabilidad.
- Alquiler de salones

Otros beneficios es la integración automática de toda la información que se genera
desde cada uno de los centros de planeación:
	- Compras, Inventarios, Producción, Administración del Recurso Humano, 
	  CRM, Gestión del Mantenimiento, Módulo ISO 9000.



Booking Workflow

Draft (Borrador)   --->   Confirmed (Confirmado)   --->   No Submit (No llegó)
		    				   --->   Submit (Llegó)
			     			   --->   Canceled (Canceló) 

		   --->   Canceled
