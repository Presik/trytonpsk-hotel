# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pyson import Eval
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateTransition

STATES = {'invisible': (Eval('type') != 'service')}


class HousekeepingScheduleStart(ModelSQL, ModelView):
    "Housekeeping Schedule Start"
    __name__ = "hotel.housekeeping.schedule.start"
    employee = fields.Many2One('company.employee', 'Employee', required=True)
    rooms = fields.Many2Many('hotel.room', None, None, 'Rooms', required=True,
        domain=[])


class HousekeepingSchedule(Wizard):
    'Housekeeping Schedule'
    __name__ = 'hotel.housekeeping.schedule.wizard'
    start = StateView(
        'hotel.housekeeping.schedule.start',
        'hotel.housekeeping_schedule_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok'),
        ]
    )
    accept = StateTransition()

    def transition_accept(self):
        Rooms = Pool().get('hotel.room')
        Rooms.write(list(self.start.rooms), {
            "housekeeping": self.start.employee.id,
        })
        return 'end'
