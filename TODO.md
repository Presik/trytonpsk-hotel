# FOLIOS

Agregar la tabla que controla los dias de ocupación, enlazada al folio, esta
sera usada para estimar la ocupación y liquidar las fechas de ingresos desde
la auditoria nocturna

## Campos

model: hotel.folio.occupancy

- folio
- occupancy_date
- unit_price
- price_list
- state: draft, confirmed, invoiced, cancelled
- charge

# PLAN TARIFARIO

Al igual que en las OTAS en Tryton Hotel las tarifas son una combinación de precios y restricciones.

• Min estancia
• Max estancia
• Min antelación
• Min estancia a la llegada
• Max estancia a la llegada
• Cierre
• Max antelación
• No OTA

Debe permitir realizar los cambios precios y restricciones por día o periodo o temporada en cada tarifa existente.

Una tarifa (creada por precios y restricciones) está asociada a condiciones y políticas, como:

Regímen: Desayuno, Media Pensión, Solo Alojamiento, etc.
Política de Cancelación: 7 días antes de la llegada, No Reembolsable, Reembolso Parcial, etc.

Moneda: En caso de ofrecer en diferentes monedas.

Derivación de tarifas

Los precios y restricciones de una tarifa se pueden calcular o actualizar de forma automática a partir de
otra tarifa existente con cálculos automáticos de precio (-%; +%, -$, +$) y las restricciones pueden ser
todas iguales, algunas iguales o ninguna igual entre tarifa y tarifa.
Así como se pueden hacer derivaciones o cálculos de precio automáticos entra tarifas, lo mismo se
puede hacer entre habitaciones.
Por ejemplo, si tienes una habitación Doble que también ofreces como Triple, podrías decir que:
Precio Triple = Precio Doble + 20%

Y, utilizando este ejemplo, digamos que también tienes una tarifa "Media Pensión"que es 50.000cop
más cara que la estándar en Solo Alojamiento.

En este caso el sistema debe hacer dos cálculos automáticos:
si el precio solo alojamiento doble es de 100.000cop entonces primero se calcula el precio Solo
alojamiento Triple (100.000 + 20% = 120.000)
después se calcula el precio Media Pensión (120.000cop + 50.000cop = 170.000cop)
