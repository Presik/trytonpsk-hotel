# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    mig_code = fields.Char('Migration Code')
    city_code = fields.Char('City Code')
    rnt_code = fields.Char('Registro Nacional de Turismo')
    matricula_mercantil = fields.Char('Matricula Mercantil')
    logo_link = fields.Char('Logo Link')
